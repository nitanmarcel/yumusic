# YuMusic

Youtube Music web app based on the desktop version with optimizations for mobile devices.

## License

Copyright (C) 2022  Marcel Alexandru Nitan

Licensed under the MIT license
