import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Window 2.2
import Morph.Web 0.1 // import "UCSComponents"
import QtWebEngine 1.7
import Qt.labs.settings 1.0
import QtSystemInfo 5.5
import Ubuntu.Components.ListItems 1.3 as ListItemm
import Ubuntu.Content 1.3

MainView {
    id : window
    objectName : "mainView"
    theme.name : "Ubuntu.Components.Themes.SuruDark"
    width : units.gu(45)
    height : units.gu(75)
    applicationName : "yumusic.nitanmarcel"
    backgroundColor : "#000000"
    WebEngineView {
        id : webview
        anchors.fill : parent
        focus : true
        zoomFactor : 2.5
        property var currentWebview: webview
        settings.pluginsEnabled : true
        profile : WebEngineProfile {
            id : webContext
            storageName : "Storage"
            persistentStoragePath : "/home/phablet/.cache/yumusic.nitanmarcel/yumusic.nitanmarcel/QtWebEngine"
        }
        url : "https://music.youtube.com/"
        userScripts : [WebEngineScript {
                injectionPoint : WebEngineScript.DocumentCreation
                worldId : WebEngineScript.MainWorld
                name : "QWebChannel"
                sourceUrl : "ubuntutheme.js"
            }
        ]
    }
    ScreenSaver {
        id : screenSaver
        screenSaverEnabled : !(Qt.application.active) || !webview.recentlyAudible
    }
}