// ==UserScript==
// @name          YoutubeMusic (YuMusic)
// @namespace     http://music.youtube.com
// @description	  Youtube Music web app based on the desktop version with optimizations for mobile devices.with some small modifications and changes to make it usable on mobile..
// @author        nitanmarcel
// @version       0.20150805144242
// ==/UserScript==


window.addEventListener("load", function(event) {
  console.log("Loaded");
  main();
});

main();

function main() {

    // Disable volume slider in expanded controls menu
    document.getElementsByClassName("expand-volume-slider style-scope ytmusic-player-bar")[0].style.display = 'none';

    // Always expand the volume controls
    var ytMusicPlayerBar = document.getElementById("expanding-menu");
    ytMusicPlayerBar.opacity = "0.1";
    ytMusicPlayerBar.ariaHidden = "false";
    ytMusicPlayerBar.ariaExpanded = "true";
    ytMusicPlayerBar.style.outline = "none";
    ytMusicPlayerBar.style.boxSizing = "border-box";
    ytMusicPlayerBar.style.maxHeight = "32px";
    ytMusicPlayerBar.style.maxWidth = "300.4px";
    ytMusicPlayerBar.style.opacity = "100"
    ytMusicPlayerBar.style.removeProperty("display");
    
    // Disable the expand button
    document.getElementsByClassName("expand-button style-scope ytmusic-player-bar")[0].style.display = "none";
  
  
    //Force audio only to avoid possible issues with the video player.

    document.getElementsByClassName("av-toggle style-scope ytmusic-av-toggle")[0].style.display = 'none';
    
    var ytMusicAvToggleDiv = document.getElementById("av-id");
    
    function mutcallback(mutations) {
        mutations.forEach(
            function iterator( value, index, collection ) {
                if (value.type == "attributes" && value.attributeName == "playback-mode") {
                    if (value.target.getAttribute(value.attributeName) == "OMV_PREFERRED") {
                        document.getElementsByClassName("song-button style-scope ytmusic-av-toggle")[0].click();
                    }
                }
                
            });
    }
    
    let observer = new MutationObserver(mutcallback);
    observer.observe(ytMusicAvToggleDiv, {attributes: true, childList: true, subtree: true});
  
}

